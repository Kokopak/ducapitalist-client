import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NbThemeModule, 
  NbLayoutModule, 
  NbSidebarModule, 
  NbMenuModule, 
  NbCardModule, 
  NbUserModule,
  NbInputModule,
  NbDialogModule,
  NbButtonModule } from '@nebular/theme';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { ProductComponent } from './product/product.component';
import { ManagerModalComponent } from './manager-modal/manager-modal.component';
import { UnlocksModalComponent } from './unlocks-modal/unlocks-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ManagerModalComponent,
    UnlocksModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbCardModule,
    NbUserModule,
    NbInputModule,
    NbDialogModule.forRoot(),
    NbButtonModule,
    FormsModule,
    ToastrModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ManagerModalComponent,
    UnlocksModalComponent
  ]
})
export class AppModule { }
