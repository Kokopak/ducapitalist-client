declare var require;
const ProgressBar = require('progressbar.js');

import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { Product, Pallier } from '../world';
import { RestserviceService } from '../restservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})


export class ProductComponent implements OnInit {

  
  product: Product;


  progressbar: any;
  @ViewChild('bar') progressBarItem;

  valueProgressBar: number = 0;

  @Input() 
  set prod(value: Product){
    this.product = value;
    this.lastupdate = Date.now();
  };

  qtList: any[] = [1, 10, 100, 'MAX'];
  @Input() multiplierIndex : number;
  @Input() money: number;
  
  @Output() notifyProduction = new EventEmitter<Product>();
  @Output() onBuy = new EventEmitter<Object>();


  maxBuy: number;

  imageUrl = "";

  // Variable temps
  lastupdate = 0;

  constructor(private service: RestserviceService, private toastr: ToastrService) { 
    this.imageUrl = service.getServer();
  }

  ngOnInit() {
    if(this.product != undefined) {
      setInterval(() => {this.calcScore()}, 100);
      this.progressbar = new ProgressBar.Line(this.progressBarItem.nativeElement, {easing: 'linear', trailColor: '#eee', color: '#9c27b0'})
      if(this.product.managerUnlocked) {
        this.startFabrication();
      }
    }
  }

  ngAfterViewInit() {
    if(this.product && this.product.timeleft > 0) {
      this.valueProgressBar = (this.product.vitesse - this.product.timeleft) / this.product.vitesse;
      this.progressbar.set(this.valueProgressBar);
      this.progressbar.animate(1, {duration: this.product.timeleft});
    }    
  }

  startFabrication() {
    if(this.product.timeleft <= 0) {
      this.progressbar.animate(1, {duration: this.product.vitesse});
      if(!this.product.managerUnlocked) {
        this.service.putProduct(this.product);
      }
      this.product.timeleft = this.product.vitesse;
      this.lastupdate = Date.now();      
    } 
  }
  
  buyProduct() {
      this.onBuy.emit({'cout': this.calcCout(), 'product': this.product, 'newQuantite': this.product.quantite + this.qtList[this.multiplierIndex]});
      this.product.quantite += this.qtList[this.multiplierIndex];

      this.service.putProduct(this.product);

      for(let p of this.product.palliers.pallier) {
        if(this.product.quantite >= p.seuil) {
          if(!p.unlocked) {
            this.applyPallier(p);
            this.toastr.success(this.product.name + ": " + p.name, 'Nouvel album !');
          }
        }
      }
  }

  calcScore() {
    let now = Date.now();
    let elapsedTime = now - this.lastupdate;
    this.maxBuy = this.calcMaxCanBuy();
    this.lastupdate = now;
    
    if (this.product.timeleft > 0) {
      this.product.timeleft -= elapsedTime;  

      if(this.product.timeleft <= 0) {
        this.notifyProduction.emit(this.product);    
        this.product.timeleft = 0;
        this.progressbar.set(0);
      }
    }

    if(this.product.managerUnlocked) {
      this.startFabrication();
    }
  }

  applyPallier(pallier: Pallier) {
    pallier.unlocked = true;
    if(pallier.typeratio == 'gain') {
      this.product.revenu *= pallier.ratio;
    }
    else if(pallier.typeratio == 'vitesse') {
      this.product.vitesse /= pallier.ratio;
      if(this.product.timeleft > 0) {
        this.product.timeleft /= pallier.ratio;
        this.progressbar.animate(1, {duration: this.product.timeleft});
      }
    }
  }

  calcCout(): number {
    if(this.multiplierIndex == 0) {
      return this.product.cout * Math.pow(this.product.croissance, this.product.quantite);
    }
    else if(this.multiplierIndex == 1) {
      return (this.product.cout * Math.pow(this.product.croissance, this.product.quantite)) * (1-Math.pow(this.product.croissance, 10)) / (1-this.product.croissance);
    }
    else if(this.multiplierIndex == 2) {
      return (this.product.cout * Math.pow(this.product.croissance, this.product.quantite)) * (1-Math.pow(this.product.croissance, 100)) / (1-this.product.croissance);
    }
    else {
      return (this.product.cout * Math.pow(this.product.croissance, this.product.quantite)) * (1-Math.pow(this.product.croissance, this.maxBuy)) / (1-this.product.croissance)
    }
  }

  calcMaxCanBuy(): number {
    let calc = Math.floor(((Math.log(-this.money*(1-this.product.croissance)+(this.product.cout*Math.pow(this.product.croissance, this.product.quantite))) - Math.log(this.product.cout*Math.pow(this.product.croissance, this.product.quantite))) / Math.log(this.product.croissance)));
    this.qtList[3] = calc;
    return calc;
  }
}