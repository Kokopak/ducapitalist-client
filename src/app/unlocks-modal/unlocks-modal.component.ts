import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Pallier } from '../world';
import { RestserviceService } from '../restservice.service';

@Component({
  selector: 'app-unlocks-modal',
  templateUrl: './unlocks-modal.component.html',
  styleUrls: ['./unlocks-modal.component.css']
})
export class UnlocksModalComponent implements OnInit {

  unlocks: any[];
  allUnlocks: boolean;
  
  imageUrl: string;

  constructor(service: RestserviceService, protected ref: NbDialogRef<UnlocksModalComponent>) { 
    this.imageUrl = service.getServer();
  }

  ngOnInit() {
  }

  dismiss() {
    this.ref.close();
  }

}
