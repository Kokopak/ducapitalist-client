import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnlocksModalComponent } from './unlocks-modal.component';

describe('UnlocksModalComponent', () => {
  let component: UnlocksModalComponent;
  let fixture: ComponentFixture<UnlocksModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnlocksModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnlocksModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
