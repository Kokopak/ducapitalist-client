import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { World, Pallier, Product } from './world';

@Injectable({
  providedIn: 'root'
})

export class RestserviceService {

  server = "http://localhost:8080/";
  user = "";

  constructor(private http: HttpClient) { }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }

  private setHeaders(user: string): HttpHeaders{
    var headers = new HttpHeaders({"X-User": user});

    return headers;
  }

  getWorld(): Promise<World> {
    return this.http.get(this.server + "ducapitalist/api/world", {headers: this.setHeaders(this.user)}).toPromise().catch(this.handleError);
  }

  putProduct(product: Product): Promise<any> {
    return this.http.put(this.server + "ducapitalist/api/product", product, {headers: this.setHeaders(this.user)}).toPromise();
  }

  putManager(manager: Pallier): Promise<any> {
    return this.http.put(this.server + "ducapitalist/api/manager", manager, {headers: this.setHeaders(this.user)}).toPromise();
  }

  getServer(): string {
    return this.server;
  }

  getUser(): string {
    return this.user;
  }

  setUser(newUser: string): void {
    this.user = newUser;
  }
}
