import { Component, ViewChildren, QueryList } from '@angular/core';

import { RestserviceService } from './restservice.service';
import { World, Pallier, Product } from './world';
import { NbDialogService } from '@nebular/theme';
import { ManagerModalComponent } from './manager-modal/manager-modal.component';
import { UnlocksModalComponent } from './unlocks-modal/unlocks-modal.component';
import { ProductComponent } from './product/product.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'ducapitalist-client';

  @ViewChildren(ProductComponent) productsComponent: QueryList<ProductComponent>;
  world: World = new World();

  server: string;
  username: string;

  score = 0;

  multiplierText: string;
  multiplierTexts: string[] = ['X1', 'X10', 'X100', 'MAX'];
  multiplierTextIndex: number = 0;

  items = [
    {
      title: 'Unlocks',
      link: [],
    },
    {
      title: 'Cash Upgrades',
      link: [],
    },
    {
      title: 'Angel Upgrades',
      link: [],
    },
    {
      title: 'Managers',
      link: [],
    },
    {
      title: 'Investors',
      link: [],
    }
  ];

  constructor(private service: RestserviceService, private dialogService: NbDialogService, private toastrService: ToastrService) {
    this.server = service.getServer();

    this.username = localStorage.getItem("username");
    if (!this.username) {
      localStorage.setItem("username", "Captain"+Math.floor(Math.random() * 1000));
    }
    this.service.setUser(this.username);

    service.getWorld().then(
      world => {
        this.world = world;
      }
    );

    this.multiplierText = this.multiplierTexts[this.multiplierTextIndex];

  }

  onUsernameChanged() {
    localStorage.setItem("username", this.username);
    this.service.setUser(this.username);

    this.service.getWorld().then(
      world => {
        this.world = world;
      }
    );
  }

  onProductionDone(p: Product) {
    this.world.money += p.quantite * p.revenu;
  }

  onProductBuy(object: Object) {
    this.world.money -= object['cout'];

    let sumQuantite = 0;
    for(let product of this.world.products.product) {
      if(product != object['product']) {
        sumQuantite += product.quantite;
      }
      else {
        sumQuantite += object['newQuantite'];
      }
    }

    let unlockedUnlock = this.world.allunlocks.pallier.find((e) => !e.unlocked && sumQuantite >= e.seuil);

    if(unlockedUnlock) {
      unlockedUnlock.unlocked = true;
      this.productsComponent.forEach(product => product.applyPallier(unlockedUnlock));
      this.toastrService.success(unlockedUnlock.name, 'Nouveau battle !');
    }

    
  }

  changeMultiplier() {
    this.multiplierTextIndex = (this.multiplierTextIndex + 1) % this.multiplierTexts.length;
    this.multiplierText = this.multiplierTexts[this.multiplierTextIndex];
  }

  showManagerDialog() {
    let dialog = this.dialogService.open(ManagerModalComponent, {
      context: {
        managerList: this.world.managers,
        products: this.world.products.product,
        money: this.world.money
      }
    });

    dialog.componentRef.instance.onBuyManager.subscribe((manager: Pallier) => {
      this.world.money -= manager.seuil;
    });
  }

  showUnlocksDialog(allUnlocks: boolean) {
    let unlocks: any[] = [];
    if(!allUnlocks) {
      for(let product of this.world.products.product) {
        let unlockedUnlock = product.palliers.pallier.find(e => !e.unlocked);
        if(unlockedUnlock) {
          unlocks.push({"product": product, "this":  unlockedUnlock});
        }
      }        
    }
    else {
      for(let unlock of this.world.allunlocks.pallier) {
        unlocks.push(unlock);
      }
    }

    let dialog = this.dialogService.open(UnlocksModalComponent, {
      context: {
        unlocks: unlocks,
        allUnlocks: allUnlocks
      }
    });

  }

}
