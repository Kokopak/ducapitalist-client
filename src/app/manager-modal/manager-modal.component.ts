import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { RestserviceService } from '../restservice.service';

import { Pallier, Product } from '../world';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'nb-dialog',
  templateUrl: './manager-modal.component.html'
})
export class ManagerModalComponent implements OnInit {

  @Input() managerList;
  @Input() money;
  @Input() products;

  onBuyManager = new EventEmitter<Pallier>();

  imageUrl: string;

  constructor(private service: RestserviceService, protected ref: NbDialogRef<ManagerModalComponent>, private toastr: ToastrService) { 
    this.imageUrl = service.getServer();
  }

  ngOnInit() {
  }

  buyManager(manager: Pallier) {
    manager.unlocked = true;
    let p: Product;

    for(let product of this.products) {
      if(manager.idcible == product.id) {
        product.managerUnlocked = true;
        p = product;
      }
    }

    this.service.putManager(manager);
    this.onBuyManager.emit(manager);

    this.toastr.success(manager.name + ' compose pour ' + p.name, 'Nouveau beatmaker !')

  }

  dismiss() {
    this.ref.close();
  }
}
